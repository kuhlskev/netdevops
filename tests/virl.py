import requests

def log(msg):
    print(msg)


class Virl(object):
    def __init__(self, ip, port='19399', username='guest', password='guest'):
        self.session = requests.Session()
        self.session.verify = False
        
        self.base_url = "http://%s:%s" % (ip, port)
        self.username = username
        self.password = password
        self.session.auth = (username, password)
        
    def api_call(self, method, query, json=None, data=None, content_type=None):
        """
        Make the VIRL API call. Returns (ok, json)
        """
        if content_type:
            headers = {'Content-Type': content_type}
        else: 
            headers = {}
        
        response = self.session.request(method, self.base_url + query, json=json, data=data, headers=headers)
        
        if response.status_code >= 200 and response.status_code < 300:
            try:
                return True, response.json()
            except:
                return True, None
        else:
            log("API call returned error: %s" % response.content)
            try:
                return False, response.json()
            except:
                return False, None
                
        # Should never make it here
        return False, None
            
    def stop_simulation(self, simulation_name):
        log("Shutting down %s simulation" % simulation_name)
        
        query = "/simengine/rest/stop/%s" % simulation_name
        ok, json = self.api_call('GET', query)
        
        if not ok:
            try:
                if json['http']['status'] == 404:
                    log("stop_simulation: API response status is 404 - assuming simulation is already stopped")
                    return True
            except:
                pass
                
            log("Error stopping simulation")
            return False
        
        log("Simulation is now stopping ...")
        return True
        
        
    def simulation_running(self, simulation_name):
        log("Checking if simulation %s is running" % simulation_name)
        
        query = "/simengine/rest/status/%s" % simulation_name
        ok, json = self.api_call('GET', query)
        
        if not ok:
            log("Error when checking simulation status")
            return False
        
        if json['state'] == 'ACTIVE':
            log("Simulation is running")
            return True
        else:
            log("Simulation is not running, state = %s" % json['state'])
            return False
            
    def simulation_start(self, simulation_name, filename):
        log("Starting simulation %s from file %s" % (simulation_name, filename))
        
        query = "/simengine/rest/launch?session=%s" % simulation_name
        with open(filename, 'r') as f:
            body = f.read()
            
        ok, json = self.api_call('POST', query, data = body, content_type='text/xml')
        
        if not ok:
            log("Error when starting simulation")
            return False
        
        log("Simulation starting ..." )
        return True
        
    def simulation_details(self, simulation_name):
        """
        Return a list of nodes - each is a dict with 'reachable' and 'management_ip' keys.
        """
        log("Getting simulation details for %s" % simulation_name)
        
        query = "/roster/rest"
        ok, json = self.api_call('GET', query)
        
        if not ok or json is None:
            log("Error when getting simulation details")
            return False
        
        ret = {}
        for k in json:
            if k == 'UUID' or json[k]['simID'] != simulation_name :
                continue
            # if 'Reachable' not in json[k] or 'managementIP' not in json[k] or 'NodeName' not in json[k]:
            if 'NodeName' not in json[k]:
                continue
            if json[k]['NodeName'].startswith('~'):
                continue
                
            ret[json[k]['NodeName']] = {
                'node_name': json[k].get('NodeName'),
                'reachable': json[k].get('Reachable', False),
                'management_ip': json[k].get('managementIP', '')
            }
            
        return ret
        
    def simulation_unreachable_nodes(self, simulation_name):
        "Check if all of the simulation nodes are showing as reachable"
        nodes = self.simulation_details(simulation_name)
        
        ur = []
        for node_id, node in nodes.items():
            if node['reachable'] != True:
                ur.append(node)
        
        if len(ur) == 0:
            return None
        return ur







